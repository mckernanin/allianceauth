# Ship Replacement

Ship Replacement helps you to organize ship replacement programs (SRP) for your alliance.

![srp](/_static/images/features/apps/srp.png)

## Installation

- Add `'allianceauth.srp',` to `INSTALLED_APPS` in your `local.py`

Perform Django Maintenance and restart our Web Service and Workers.

::::{tabs}
:::{group-tab} Bare Metal

```shell
python manage.py migrate
python manage.py collectstatic --noinput
supervisorctl restart myauth:
```

:::
:::{group-tab} Containerized

```shell
docker compose --env-file=.env up -d
docker compose exec allianceauth_gunicorn bash
auth migrate
auth collectstatic
```

:::
::::

## Permissions

To use and administer this feature, users will require some of the following.

```{eval-rst}
+----------------------+------------------+------------------------------------------------------------+
| Permission           | Admin Site       | Auth Site                                                  |
+======================+==================+============================================================+
| auth.access_srp      | None             | Can create an SRP request from a fleet                     |
+----------------------+------------------+------------------------------------------------------------+
| auth.srp_management  | None             | Can Approve and Deny SRP requests, Can create an SRP Fleet |
+----------------------+------------------+------------------------------------------------------------+
| srp.add_srpfleetmain | Can Add Model    | Can Create an SRP Fleet                                    |
+----------------------+------------------+------------------------------------------------------------+
```
